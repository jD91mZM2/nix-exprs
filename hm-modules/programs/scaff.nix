{ config, pkgs, lib, inputs, system, ... }:

with lib;

let
  cfg = config.programs.scaff;

  format = pkgs.formats.toml { };

  scaff-wrapped = pkgs.runCommand "scaff"
    {
      nativeBuildInputs = with pkgs; [ makeWrapper ];
    } ''
    cp -r "${cfg.package}" "$out"
    chmod +w "$out/bin"
    wrapProgram "$out/bin/scaff" ${toString (map (config: escapeShellArgs [ "--add-flags" ("-i " + config) ]) cfg.extra-configs)}
  '';
in
{
  options = {
    programs.scaff = {
      enable = mkEnableOption "scaff";
      package = mkOption {
        type = types.package;
        default = inputs.scaff.defaultPackage."${system}";
        description = ''
          Which scaff package to use. Defaults to the latest one,
          because as I'm writing this no scaff version is in nixpkgs.
        '';
      };

      settings = mkOption {
        type = types.nullOr format.type;
        default = null;
        example = literalExample ''
          {
            my-local-template          = ./my-local-template.tar.gz;
            my-remote-template         = "https://example.org/my-remote-template.tar.gz";
            my-remote-template-fetched = builtins.fetchurl https://example.org/my-remote-template-built-using-nix.tar.gz;
          }
        '';
        description = ''
          Generates a config using Nix. Provide an attribute of alias ->
          imported tarballs, which will be available by using `scaff <alias>`
        '';
      };

      extra-configs = mkOption {
        type = types.coercedTo types.str singleton (types.listOf types.str);
        default = [ ];
        example = "https://gitlab.com/jD91mZM2/scaff-repo/-/jobs/836232426/artifacts/raw/build/config.toml";
        description = ''
          Input a single or a list of additional configs to import. These will
          be added using the `-i` option when invoking scaff.
        '';
      };
    };
  };
  config = mkIf cfg.enable {
    home.packages = [
      scaff-wrapped
    ];

    xdg.configFile."scaff/config.toml" = mkIf (cfg.settings != null) {
      source = format.generate "scaff-config.toml" cfg.settings;
    };
  };
}
