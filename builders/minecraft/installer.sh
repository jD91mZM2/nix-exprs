#!/usr/bin/env bash

MC_DATAPACK_ZIP="${MC_DATAPACK_ZIP?-This environment variable should be prepended by Nix}"
MC_DATAPACK_NAME="${MC_DATAPACK_NAME?-This environment variable should be prepended by Nix}"

if [ -z "$1" ]; then
  echo "Usage: installer.sh <world directory>" >&2
  echo >&2
  echo "Please supply a world directory as input to this command line." >&2
  echo "Example: ~/.minecraft/saves/my-world" >&2
  exit 1
fi

world="$1"

if ! [ -d "$world/datapacks" ]; then
  echo "The pointed-out world directory seems corrupt, it's missing a 'datapacks' folder" >&2
  echo "Are you sure it's pointing to a minecraft world directory?" >&2
  echo "Example: ~/.minecraft/saves/my-world" >&2
  exit 1
fi

dest="$world/datapacks/$MC_DATAPACK_NAME"

if [ -e "$dest" ]; then
  echo "Overwriting existing file $MC_DATAPACK_NAME"
fi

install "$MC_DATAPACK_ZIP" "$dest"
