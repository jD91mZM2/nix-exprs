{ pkgs, nix-exprs, lib, config, ... }:

with lib;
let
  cfg = config.datapack;

  namespaceSubmodule = types.submodule ([
    {
      # Forward nix-exprs attribute to submodule
      _module.args = { inherit nix-exprs; };
    }
    ./namespace.nix
  ]);
in
{
  options.datapack = {
    # Data in the pack.mcmeta.
    name = mkOption {
      type = types.str;
      description = "The name of your datapack";
      default = "my-datapack";
    };
    description = mkOption {
      type = types.attrs;
      description = "The description of your datapack";
      default = {
        text = "Nix-generated datapack";
        color = "yellow";
      };
    };

    # data/ directory
    namespaces = mkOption {
      type = types.attrsOf namespaceSubmodule;
      description = "The namespaces in this datapack";
      default = { };
    };
    additions = mkOption {
      type = namespaceSubmodule;
      description = "Shorthand for namespaces.\${name}";
      default = { };
    };
    modifications = mkOption {
      type = namespaceSubmodule;
      description = "Shorthand for namespaces.minecraft";
      default = { };
    };
  };

  config = mkMerge [
    {
      datapack.namespaces = {
        "${cfg.name}" = cfg.additions;
        minecraft = cfg.modifications;
      };

      outputName = "${cfg.name}";

      file = {
        "pack.mcmeta".text = builtins.toJSON {
          pack = {
            description = cfg.description;
            pack_format = 6;
          };
        };
      };
    }
    {
      file = nix-exprs.lib.mergeMapAttrs
        (nsname: ns: mapAttrs'
          (filename: content:
            nameValuePair "data/${nsname}/${filename}" {
              text = content;
            }
          )
          ns.file)
        cfg.namespaces;
    }
  ];
}
