{ pkgs, lib, self }:

with lib;
rec {
  # Create an uncompressed datapack
  mkDatapackUncompressed = self.lib.builders.files
    [ ./datapack.nix ./function.nix ]
    pkgs;

  # Create a zipped datapack
  mkDatapack = modules:
    let
      datapack = mkDatapackUncompressed modules;
      drv = pkgs.runCommand "${datapack.name}.zip"
        {
          passthru = {
            installer = pkgs.writeShellScriptBin "installer.sh" ''
              export MC_DATAPACK_ZIP="${drv}"
              export MC_DATAPACK_NAME="${drv.name}"
              exec "${./installer.sh}" "$@"
            '';
          };
        }
        ''
          cd ${escapeShellArg datapack}
          "${pkgs.zip}/bin/zip" "$out" -r *
        '';
    in
    drv;

  # Create layer for superflat world
  mkLayer = height: block: {
    inherit height block;
  };

  # Create a dimension generator for a superflat world
  genSuperflat =
    { biome ? "minecraft:plains"
    , layers ? [
        (mkLayer 1 "minecraft:bedrock")
        (mkLayer 2 "minecraft:dirt")
        (mkLayer 1 "minecraft:grass_block")
      ]
    ,
    }:
    {
      type = "minecraft:flat";
      settings = {
        inherit biome layers;
        structures = {
          structures = { };
        };
      };
    };

  # Create a dimension generator for a normal world
  genNormal =
    { setting ? "minecraft:overworld"
    , seed
    ,
    }:
    {
      type = "minecraft:noise";
      inherit seed;
      settings = setting;
      biome_source = {
        type = "minecraft:vanilla_layered";
        inherit seed;
        large_biomes = false;
        legacy_biome_init_layer = false;
      };
    };

  # Preset dimension settings
  # See https://minecraft.gamepedia.com/Custom_dimension#Defaults
  dimensionSettings = {
    overworld = {
      ultrawarm = false;
      natural = true;
      coordinate_scale = 1.0;
      piglin_safe = false;
      respawn_anchor_works = false;
      bed_works = true;
      has_raids = true;
      has_skylight = true;
      has_ceiling = false;
      # fixed_time = N/A;
      ambient_light = 0.0;
      logical_height = 256;
      infiniburn = "minecraft:infiniburn_overworld";
    };
    nether = {
      ultrawarm = true;
      natural = false;
      coordinate_scale = 8.0;
      piglin_safe = true;
      respawn_anchor_works = true;
      bed_works = false;
      has_raids = false;
      has_skylight = false;
      has_ceiling = true;
      fixed_time = 18000;
      ambient_light = 0.1;
      logical_height = 128;
      infiniburn = "minecraft:infiniburn_nether";
    };
    end = {
      ultrawarm = false;
      natural = false;
      coordinate_scale = 1.0;
      piglin_safe = false;
      respawn_anchor_works = false;
      bed_works = false;
      has_raids = true;
      has_skylight = false;
      has_ceiling = false;
      fixed_time = 6000;
      ambient_light = 0.0;
      logical_height = 256;
      infiniburn = "minecraft:infiniburn_end";
    };
  };
}
