{ config, nix-exprs, lib, ... }:
with lib;
let
  toTag = list: {
    replace = false;
    values = list;
  };
in
{
  options = {
    file = mkOption {
      type = types.attrsOf types.str;
      description = "The raw files";
      default = { };
    };

    advancements = mkOption {
      type = types.attrsOf types.attrs;
      description = "Advancements";
      default = { };
    };
    functions = mkOption {
      type = types.attrsOf types.lines;
      description = "Functions";
      default = { };
    };
    lootTables = mkOption {
      type = types.attrsOf types.attrs;
      description = "Loot Tables";
      default = { };
    };
    predicates = mkOption {
      type = types.attrsOf types.attrs;
      description = "Predicates";
      default = { };
    };
    recipes = mkOption {
      type = types.attrsOf types.attrs;
      description = "Recipes";
      default = { };
    };
    tags = mkOption {
      type = types.attrsOf (
        types.attrsOf (
          types.coercedTo (types.listOf types.str) toTag (types.submodule {
            options = {
              replace = mkOption {
                type = types.bool;
                description = "Whether or not to replace the existing functions in this tag";
                default = false;
              };
              values = mkOption {
                type = types.listOf types.str;
                description = "Functions in this tag";
              };
            };
          })
        )
      );
      description = "Function Tags";
      default = { };
    };
    dimensionTypes = mkOption {
      type = types.attrsOf types.attrs;
      description = "Dimension types";
      default = { };
    };
    dimensions = mkOption {
      type = types.attrsOf types.attrs;
      description = "Dimensions";
      default = { };
    };
  };
  config = mkMerge [
    {
      file = mapAttrs'
        (name: value: nameValuePair "advancements/${name}.json" (builtins.toJSON value))
        config.advancements;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "functions/${name}.mcfunction" value)
        config.functions;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "loot_tables/${name}.json" (builtins.toJSON value))
        config.lootTables;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "predicates/${name}.json" (builtins.toJSON value))
        config.predicates;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "recipes/${name}.json" (builtins.toJSON value))
        config.recipes;
    }
    {
      file = nix-exprs.lib.mergeMapAttrs
        (kind: subsets: mapAttrs'
          (name: value: nameValuePair "tags/${kind}/${name}.json" (builtins.toJSON value))
          subsets)
        config.tags;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "dimension_type/${name}.json" (builtins.toJSON value))
        config.dimensionTypes;
    }
    {
      file = mapAttrs'
        (name: value: nameValuePair "dimension/${name}.json" (builtins.toJSON value))
        config.dimensions;
    }
  ];
}
