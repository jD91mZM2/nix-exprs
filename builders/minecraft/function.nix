{ lib, config, ... }:
with lib;
let
  cfg = config.function;

  setups = mapAttrsToList (_name: f: f.setup) cfg;
  teardowns = mapAttrsToList (_name: f: f.teardown) cfg;

  mergeTags = tagsFor:
    # merged { "<tag>" = [ "datapack:name" ]; }
    mkMerge
      # many [{ "<tag>" = [ "datapack:name" ]; }]
      (mapAttrsToList
        # one { "<tag>" = [ "datapack:name" ]; }
        (name: f: listToAttrs
          # { name = "<tag>"; value = "datapack:name"; }
          (map
            (tag: nameValuePair tag [ "${config.datapack.name}:${name}" ])
            (tagsFor f))
        )
        cfg);
in
{
  options.function = mkOption {
    type = types.attrsOf (types.submodule {
      options = {
        tags = {
          minecraft = mkOption {
            type = types.listOf types.str;
            description = "List of minecraft tags to add this function to";
            default = [ ];
          };
          custom = mkOption {
            type = types.listOf types.str;
            description = "List of custom tags to add this function to";
            default = [ ];
          };
        };
        setup = mkOption {
          type = types.lines;
          description = "Startup code required to setup all necessary resources (scoreboards, etc)";
          default = "";
        };
        teardown = mkOption {
          type = types.lines;
          description = "Code to be added to a special uninstall function";
          default = "";
        };
        code = mkOption {
          type = types.lines;
          description = "Function code";
          default = "";
        };
      };
    });
    description = "Minecraft Functions";
    default = { };
  };

  config.datapack = mkMerge [
    # Setup code required
    (mkIf (any (setup: setup != "") setups) {
      additions.functions.install = toString setups;
      modifications.tags.functions.load = [ "${config.datapack.name}:install" ];
    })
    # Teardown code required
    (mkIf (any (teardown: teardown != "") teardowns) {
      additions.functions.uninstall = toString teardowns;
    })
    # The functions themselves
    {
      additions = {
        functions = mapAttrs (_name: f: f.code) cfg;
        tags.functions = mergeTags (f: f.tags.custom);
      };

      modifications.tags.functions = mergeTags (f: f.tags.minecraft);
    }
  ];
}
