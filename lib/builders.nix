{ self, lib, ... }:

with lib;
rec {
  basic = builderModules: pkgs: userModules: (evalModules {
    modules = toList builderModules ++ toList userModules ++ (singleton {
      options.outputDrv = mkOption {
        type = types.package;
        description = "The output derivation of this builder";
      };
      config._module.args = {
        inherit pkgs;
        nix-exprs = self;
      };
    });
  }).config.outputDrv;

  command = modules: basic (toList modules ++ (singleton ({ pkgs, config, ... }: {
    options = {
      outputName = mkOption {
        type = types.str;
        description = "Output drv's name";
        default = "";
      };
      outputScript = mkOption {
        type = types.lines;
        description = "Script to run to create drv";
        default = "";
      };
    };
    config.outputDrv = pkgs.runCommand config.outputName { } ''
      mkdir "$out"

      ${config.outputScript}
    '';
  })));

  files = modules: command (toList modules ++ (singleton ({ pkgs, config, ... }: {
    options.file = mkOption {
      type = types.attrsOf (types.submodule ({ name, config, ... }: {
        options = {
          source = mkOption {
            type = types.path;
            description = "Source file to copy";
          };
          text = mkOption {
            type = types.lines;
            description = "File contents (can't be used with source)";
            default = "";
          };
          permission = mkOption {
            type = types.int;
            description = "File permission (as octal - will be converted to string)";
            default = 0755;
          };
        };
        config = mkIf (config.text != "") {
          source = pkgs.writeText name config.text;
        };
      }));
      description = "Files to create";
      default = { };
    };
    config = {
      outputScript = toString (
        mapAttrsToList
          (filename: file: ''
            install -Dm ${toString file.permission} ${escapeShellArg file.source} "$out"/${escapeShellArg filename}
          '')
          config.file
      );
    };
  })));
}
