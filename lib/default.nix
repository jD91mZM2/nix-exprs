{ self, lib, ... }:

with lib;
let
  args = { inherit self lib; };
  builders = import ./builders.nix args;
  strings = import ./strings.nix args;

  selfLib = rec {
    inherit (strings) simpleSplit stringFilter toCamelCase;
    inherit builders;

    # A variation of sourceByRegex that excludes files based on regex rather than includes them.
    sourceByNotRegex = src: excludes: (
      let
        shouldKeep = path: all (regex: builtins.match regex path == null) excludes;
      in
      cleanSource (
        cleanSourceWith {
          filter = path: _type: shouldKeep path;
          inherit src;
        }
      )
    );

    # Clean rust sources: Remove `target/` as well as any sqlite databases.
    cleanSourceRust = src: sourceByNotRegex src [
      ''^(.*/)?target(/.*)?$''
      ''^.*\.sqlite$''
    ];

    # Convert to hex digits
    toHexDigit = digit:
      if digit < 10 then
        toString digit
      else
        builtins.elemAt [ "A" "B" "C" "D" "E" "F" ] (digit - 10);

    # Naive merging of a list of attribute sets
    mergeAttrs = foldl' (a: b: a // b) { };

    # Map each key-value pair into an attribute set, and merge them all into one
    mergeMapAttrs = f: attrs: mergeAttrs (mapAttrsToList f attrs);
  };
in
# simple tests can go here
assert selfLib.stringFilter "[^a-zA-Z0-9]" "Hello, world! The sun's beautiful today, 'innit?" == "HelloworldThesunsbeautifultodayinnit";
assert selfLib.toCamelCase "Hello, world!" == "helloWorld";

assert selfLib.toHexDigit 15 == "F";
assert selfLib.mergeAttrs [
  { a = "overwritten"; }
  { b = "keep this"; }
  { a = "new value"; }
] == {
  a = "new value";
  b = "keep this";
};

selfLib
