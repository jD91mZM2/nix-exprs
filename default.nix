let
  flake = (import (builtins.fetchTarball "https://github.com/edolstra/flake-compat/archive/master.tar.gz") {
    src = ./.;
  }).defaultNix.outputs;
in
flake // {
  modules = flake.nixosModules;
  hm-modules = flake.hmModules;
}
