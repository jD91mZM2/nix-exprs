{
  description = "Miscellaneous nix expressions";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    scaff.url = "gitlab:jD91mZM2/scaff";
    st.url = "gitlab:jD91mZM2/st";
    purepkgs.url = "gitlab:jD91mZM2/purepkgs";

    neovim = {
      url = "github:neovim/neovim/nightly";
      flake = false;
    };
    nvim-treesitter = {
      url = "github:nvim-treesitter/nvim-treesitter";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, st, purepkgs, ... } @ inputs:
    let
      callModule = module: {
        _module.args = {
          inherit inputs;
        };
        imports = [ module ];
      };
      moduleList = modules: modules // {
        all = {
          imports = builtins.attrValues modules;
        };
      };
    in
    {
      # Library items
      lib = import ./lib {
        inherit self;
        inherit (nixpkgs) lib;
      };

      # NixOS modules
      nixosModules = moduleList {
        custom-services = callModule ./modules/custom-services.nix;
        powerline-rs = callModule ./modules/programs/powerline-rs.nix;
        zsh-vi = callModule ./modules/programs/zsh-vi.nix;
      };

      # Home-manager modules
      hmModules = moduleList {
        firefox-privacy = callModule ./hm-modules/programs/firefox-privacy.nix;
        scaff = callModule ./hm-modules/programs/scaff.nix;
      };
    } // (utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config = {
            # TODO: tree-sitter is broken -- but still works?
            allowBroken = true;
          };
        };
      in
      rec {
        # Builders
        builders = {
          dnd = pkgs.callPackage ./builders/dnd {
            self = self;
          };
          minecraft = pkgs.callPackage ./builders/minecraft {
            self = self;
          };
        };
      }));
}
