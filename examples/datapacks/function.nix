let
  self = import ../../.;
  inherit (self.builders."x86_64-linux") minecraft;
in
minecraft.mkDatapack {
  datapack = {
    name = "minimal";
    description.text = "A test datapack";
  };
  function.health = {
    tags = {
      minecraft = [ "tick" ];
      custom = [ "wot" ];
    };

    setup = ''
      scoreboard objectives add armour dummy "Armour"
      scoreboard objectives setdisplay sidebar armour
    '';

    teardown = ''
      scoreboard objectives remove armour
    '';

    code = ''
      execute as @a store result score @s armour run attribute @s minecraft:generic.armor get
    '';
  };
}
