let
  self = import ../../.;
  inherit (self.builders."x86_64-linux") minecraft;
in
minecraft.mkDatapack {
  datapack = {
    name = "my-dimensions";
    description.text = "Adds two new dimensions";

    additions = {
      dimensionTypes = {
        type = minecraft.dimensionSettings.overworld // {
          has_skylight = false;
        };
      };
      dimensions = {
        superflat = {
          type = "my-dimensions:type";
          generator = minecraft.genSuperflat { };
        };
        normal = {
          type = "my-dimensions:type";
          generator = minecraft.genNormal {
            seed = 0;
          };
        };
      };
    };
  };
}
